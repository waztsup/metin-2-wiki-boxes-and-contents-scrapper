const fs = require('fs');
const fetch = require('node-fetch');
const { parse } = require('node-html-parser');

const outputFolder = './output';
const initialFileName = 'boxes-and-contents';
const fileExtension = 'json';
let fileVersion;

const webPage = 'https://en-wiki.metin2.gameforge.com/index.php/Boxes';
const baseUrl = 'https://en-wiki.metin2.gameforge.com';

function getFileNameWithExtension(fileName = initialFileName, extension = fileExtension, version = fileVersion.version) {
    return `${fileName} - v${version}.${extension}`;
}

function updateFileVersion() {
    fs.writeFileSync('file-version.json', JSON.stringify(fileVersion));
    console.log('File version was updated');
}

function saveData(data) {
    if (!fs.existsSync(outputFolder)) {
        fs.mkdirSync(outputFolder);
    }

    const outputPath = `${outputFolder}/${getFileNameWithExtension()}`;
    fs.writeFileSync(outputPath, data);
    console.log('Data was saved!');
    updateFileVersion();
}

fs.readFile('file-version.json', (err, data) => {
    if (err) fileVersion = 0;
    fileVersion = JSON.parse(data);
    fileVersion.version++;
});

// check if data for all boxes has been fetched
function checkBoxes(boxes) {
    let flag = true;
    try {
        boxes.forEach(box => {
            if (!box.fetched) {
                throw 'Chapter not fetched/saved.';
            }
        })
    } catch (e) {
        flag = false;
    } finally {
        return flag;
    }
}

// scrape basic item data for a given item
function scrapeItem(item) {
    const itemImg = item.querySelector('a img');
    const itemLink = item.querySelector('a');
    const itemTitle = item.innerText.trim();
    return {
        name: itemTitle,
        image: itemImg ? baseUrl + itemImg.attrs.src : undefined,
        link: itemLink ? baseUrl + itemLink.attrs.href : undefined
    };
}

// scrape category items for a given category
function scrapeCategory(category, categoryIndex, categoryTitles) {
    categoryName = categoryTitles[categoryIndex].innerText.replace('Content -', '').trim();
    return {
        name: categoryName,
        content: category
            .querySelectorAll('div dl dd dl dd')
            .map(item => {
                let itemData;
                try {
                    itemData = scrapeItem(item);
                } catch (e) {
                    console.log(`Item error: ${e.message}`);
                } finally {
                    return itemData;
                }
            })
    }
}

// scrape category titles and their context for a given box page
function scrapeCategories(boxPage) {
    const categories = boxPage.querySelectorAll('table tr:nth-child(3) td[colspan="2"] table tr:nth-child(even) td');
    const categoryTitles = boxPage.querySelectorAll('table tr:nth-child(3) td[colspan="2"] table tr:nth-child(odd) td');

    return categories.map((category, categoryIndex) => {
        return scrapeCategory(category, categoryIndex, categoryTitles)
    });
}

// scrape basic box data
function scrapeBox(box) {
    const boxImg = baseUrl + box.querySelector('td:first-child a img').attrs.src;
    const boxTitle = box.querySelector('td:nth-child(2) a');
    const boxLink = baseUrl + boxTitle.attrs.href;
    const boxName = boxTitle.innerText.trim();
    const boxSource = box.querySelector('td:nth-child(3) a');
    const boxDescription = box.querySelector('td:last-child');
    return {
        name: boxName,
        image: boxImg,
        link: boxLink,
        source: boxSource ? boxSource.innerText.trim() : undefined,
        description: boxDescription ? boxDescription.innerText.trim() : undefined,
        content: [],
        fetched: false
    };
}

fetch(webPage)
    .then(result => result.text())
    .then(body => {
        const root = parse(body);
        const data = [];
        const boxes = root.querySelectorAll('table tr:not(:first-child)');

        boxes.forEach((box, boxIndex) => {
            try {
                const boxData = scrapeBox(box);
                data[boxIndex] = boxData;

                fetch(boxData.link)
                    .then(result => result.text())
                    .then(boxBody => {
                        const boxRoot = parse(boxBody);
                        data[boxIndex].content = scrapeCategories(boxRoot);
                        data[boxIndex].fetched = true;

                        if (checkBoxes(data)) {
                            saveData(JSON.stringify(data));
                        }
                    });
            } catch (e) {
                console.log(`Box error (#${boxIndex}): ${e.message}`);
            }
        });
    });